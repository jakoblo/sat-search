package satsearch

import "time"

type Stac struct {
	StacVersion string `json:"stac_version"`
	ID          string `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Links       []struct {
		Rel  string `json:"rel"`
		Href string `json:"href"`
	} `json:"links"`
}

// BBOX: However, in cases where the box spans the antimeridian the first value (west-most box edge) is larger than the third value (east-most box edge).

type SearchParams struct {
	CollectionId string      `json:"collectionId,omitempty"`
	Bbox         []float64   `json:"bbox,omitempty"` // bbox[min long, min lat, max long, max lat]
	Time         string      `json:"time,omitempty"`
	Intersects   interface{} `json:"intersects,omitempty"`
	Query        Query       `json:"query,omitempty"`
	Limit        int         `json:"limit,omitempty"`
	Sort         []struct {
		Field     string `json:"field,omitempty"`
		Direction string `json:"direction,omitempty"`
	} `json:"sort,omitempty"`
}

type Bbox struct {
	MinLong float64
	MinLat  float64
	MaxLong float64
	MaxLat  float64
}

type Query struct {
	CloudCover struct {
		Lt int `json:"lt,omitempty"`
	} `json:"eo:cloud_cover,omitempty"`
}

type SearchResponse struct {
	Type     string `json:"type"`
	Features []struct {
		Type     string    `json:"type"`
		ID       string    `json:"id"`
		Bbox     []float64 `json:"bbox"`
		Geometry struct {
			Type        string        `json:"type"`
			Coordinates [][][]float64 `json:"coordinates"`
		} `json:"geometry"`
		Properties struct {
			Datetime time.Time `json:"datetime"`
		} `json:"properties"`
		Links []struct {
			Rel  string `json:"rel"`
			Href string `json:"href"`
		} `json:"links"`
		Assets struct {
			Analytic struct {
				Title string `json:"title"`
				Href  string `json:"href"`
				Type  string `json:"type"`
			} `json:"analytic"`
			Thumbnail struct {
				Title string `json:"title"`
				Href  string `json:"href"`
				Type  string `json:"type"`
			} `json:"thumbnail"`
		} `json:"assets"`
	} `json:"features"`
	Links []struct {
		Rel  string `json:"rel"`
		Href string `json:"href"`
	} `json:"links"`
}
