package satsearch

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// Returns the root catalog or collection.
func Root(url string) Stac {
	res, err := http.Get(url)

	if err != nil {
		log.Fatal(err)
	}

	defer res.Body.Close()

	var result Stac
	json.NewDecoder(res.Body).Decode(&result)

	return result
}

// The landing page of the API
func Index(url string) map[string]interface{} {
	res, err := http.Get(url)

	if err != nil {
		log.Fatal(err)
	}

	defer res.Body.Close()

	var result map[string]interface{}
	json.NewDecoder(res.Body).Decode(&result)

	return result
}

// Describes the feature collections in the dataset
func Collections(url string) (map[string]interface{}, error) {
	if url == "" {
		return nil, fmt.Errorf("URL is missing!")
	}
	res, err := http.Get(url)

	if err != nil {
		log.Fatal(err)
	}

	defer res.Body.Close()

	var result map[string]interface{}
	json.NewDecoder(res.Body).Decode(&result)

	return result, nil
}

// Search STAC items by full-featured filtering.
func Search(url string, p map[string]interface{}) (SearchResponse, error) {
	var result SearchResponse
	if url == "" {
		return result, fmt.Errorf("URL is missing!")
	}

	b, _ := json.Marshal(p)
	fmt.Println(string(b))
	resp, err := http.Post(url, "application/json", bytes.NewReader(b))

	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	json.NewDecoder(resp.Body).Decode(&result)
	return result, nil
}
